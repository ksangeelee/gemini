This repository holds miscellaneous files from my Gemini server's directory.

Any code that's my own (Kevin Sangeelee) should be considered licensed under GPL2 (or later, e.g. for derivative works).

Some scripts depend on binaries that are not in the repository (for html2text and Swish++). If you need these and trust my binaries, then ask on IRC (kevinsan) and I can provide either my binaries, or else my source trees to compile your own.

Similarly, if any files are obviously missing, let me know and I'll add them.

## twitter2gmi.js

Generates gemtext files containing the latest 40 tweets for the given twitter users, and writes an index with all given users listed along with their top three tweets.
```
# Generate for a single twitter user
./twitter2gmi.js --gmi-dir=~/gemini/content/tweeters ksangeelee

# Generate for all users listed in the file tweeters.txt (e.g. one per line)
./twitter2gmi.js --gmi-dir=~/gemini/content/tweeters $(cat tweeters.txt)

# Generate for a specific group of twitter users
./twitter2gmi.js --gmi-dir=~/gemini/content/tweeters --index-file=gamers.gmi somegamer83 another973
./twitter2gmi.js --gmi-dir=~/gemini/content/tweeters --index-file=chefs.gmi crazyshef838 madramsay1954
```

The code delays fetches from twitter servers to one every two seconds. Though I don't think there's any rate limits, it seems polite to fetch at something like human-rate.

There are three dependencies: bent, cheerio, and mri. These provide http requests, dom parsing, and command-line parsing respectively.

## acme.sh

**Note:** I only use a Let's Encrypt certificate because it's already on my main HTTP server *and* I use it on my Gemini HTTP server (that I run on port 1993), and so I can be sure it's valid. The transaction overhead is high for this certificate. I'll be moving back to self-signed certificates for at least my Gemini server (port 1965) and possibly the port 1993 HTTP server too. What's annoying is that browsers make a *huge* fuss about self-signed certificates.

For now, I use the main web-server's acme.sh to generate certificates for gemini.susa.net. This is because let's encrypt insists on validating via port 80 and port 443 only, and these are already being used. This is an annoyance, because it means certificate generation can't be enclosed within the gemini directory. One solution might be to use IPv6 only and use a separate IP address for Gemini on IPv6. The other solution that I know works is to use a reverse proxy on port 80 forwarding to the internal/containerised servers, in which case the standalone server can be used.

### The output from www.susa.net's acme.sh issue command.
```
    Your cert is in  /root/.acme.sh/gemini.susa.net/gemini.susa.net.cer
    Your cert key is in  /root/.acme.sh/gemini.susa.net/gemini.susa.net.key
    The intermediate CA cert is in  /root/.acme.sh/gemini.susa.net/ca.cer
    And the full chain certs is there:  /root/.acme.sh/gemini.susa.net/fullchain.cer
```

### The following information is kept for reference, but is otherwise unused.

The directory acmefiles is where acme.sh is installed. This was done by the installer available on GitHub.

```
git clone https://github.com/Neilpang/acme.sh.git
cd acme.sh
./acme.sh --install --home ~/gemini/acmefiles
```

The installer directory can be deleted at this point, since everything needed is in acmefiles.

