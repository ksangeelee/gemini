# Latest few tweets from your tweeters, fetched Sun Oct 18 2020 11:40:01 PM

=> /tweeters/tweets_mugecevik.gmi Muge Cevik (@mugecevik): Infectious Diseases / Virology...
* Quick thread on latest @ONS infection survey data, which is very informative! :: FYI: the survey involves mass random population testing for #COVID19 in England and Wales, so is not biased by symptoms/test seeking - so very useful indeed! :: 1/7 [RT]
* 🧵 I never saw a future that I'd be sad to see my article published in @TheLancet. But in many ways this feels too late. :: When the original paper came out in May, my coauthors and I were stunned. 1/ :: thelancet.com/journals/lance… [RT]
* The answer lies in the tedious, complex business of basic public health, writes the FT's @timharford  :: on.ft.com/3deK08o [RT]
* Understanding the unmet needs and providing resources to address them should be the guiding principle of COVID19 response.  :: #ResourcesBeforeRestrictions twitter.com/sdbaral/status… [RT]

=> /tweeters/tweets_Rainmaker1973.gmi Massimo (@Rainmaker1973): Astronomy, astronautics, meteo...
* Believed to be standing for thousands of years, Long Island's Balancing Rock at Digby Neck seems to defy gravity. The columnar basalt rock has defied erosion and is standing tall at about 9 meters in height [read more: buff.ly/2FDfxoe] pic.twitter.com/qOpfesw8Gd
* Street artist Pejac's “SOCIAL DISTANCING” is a trompe l'oeil that creates an illusion of a deep gaping crevice on a rigid surface of a cement wall at the campus of University Hospital Marqués de Valdecilla, Santander, made to honor Spain's health workers buff.ly/3nYIcFI pic.twitter.com/4z3t3ckBaU [RT]
* This clip shows just how giant sequoia trees are in California [source: buff.ly/2Hj95n6] [read more: buff.ly/3j9yywf] pic.twitter.com/nWos6YvAcu [RT]
* The amount of energy used by all humans and industries annually (~5.67 × 10²⁰ joules or ~567 exajoules) is equivalent to the amount of solar energy that strikes the earth in the span of only 90 minutes buff.ly/2nyMxBy | buff.ly/2nyMMws pic.twitter.com/b1n86u8Ird [RT]

=> /tweeters/tweets_henryhomesweet.gmi Homesweet (@henryhomesweet): he/him...
* Absolutely devastated to hear that my friend Ultrasyd has passed away. He was the nicest and most talented person I have ever known, I cannot believe the news 🌹 R.I.P twitter.com/cymbob/status/…
* It is with great sadness that I inform you of the brutal passing of my friend Sydney, known as Ultrasyd. Syd was not only a talented musicien but the essence of kindness and genuine humanity. pic.twitter.com/5NQhFFKE8Q [RT]
* Stevia Sphere - Software Piracy 3.5" Floppy is now LIVE! :: Our first floppy in .IT format! :: Check out this amazing album now in all it's keygen glory! :: strudelsoft.bandcamp.com/album/software… pic.twitter.com/RJKlp6Gdlb [RT]
* The latest from @henryhomesweet is truly very special. More like this from the post-chiptune massive! pic.twitter.com/D7PfeMCqmT [RT]

=> /tweeters/tweets_trvrb.gmi Trevor Bedford (@trvrb): Scientist @fredhutch, studying...
* That’s a really great figure. Could I ask for it’s reference? Also, I strongly doubt that this effect came from viral evolution. Would explanation be  that secondary infections of 15-35y cohort aren’t as deadly due to immunity after high attack rates in 1918-1919?
* Our new Situation Report is now available: :: nextstrain.org/narratives/nco… :: 8 months into the global transmission of #SARSCoV2 #COVID19, we look at the changing patterns of transmission we can see through viral genetics, including the initial fast spread & the effects of lockdown :: 1/3 pic.twitter.com/1h9GaQppOB [RT]
* I agree with this.
* This was my take as well. Tragic that we’re in this situation.
* My impression was that false positives that occasionally occur are commonly from lab handing errors (contamination / swapped samples). The background rate of positivity will affect the FPR in this case.

=> /tweeters/tweets_edyong209.gmi Ed Yong (@edyong209): Science writer at The Atlantic...
* "The route to herd immunity would run through graveyards." — @DrTomFrieden on the wrongheadedness of idea of letting #Covid19 spread unchecked.  :: It's not the only option. It's not a good option. washingtonpost.com/opinions/tom-f… [RT]
* “Do you want to have another crack at answering that?”  :: This stunning @TovaOBrien interview sets the bar. 🔥🔥🔥 twitter.com/MollyJongFast/…
* It didn't have to be this way.  :: If we act now, we have a small chance at saving it. twitter.com/aravosis/statu… [RT]
* Warning for COVID scientists: An impostor is asking scientists for an interview using the email atulgawande65@gmail.com (not mine) and signature “Atul Gawande, Staff Writer, The New Yorker.”  :: These are fake. Do not respond. I use a bwh.harvard.edu address for work. 1/4 [RT]

=> /tweeters/tweets_peripatetical.gmi Allen Cheng (@peripatetical): ID physician, epidemiologist/s...
* How do we reconcile the conflicting remdesivir study results for treatment of #COVID19? Here I give it a try. h/t @AaronRichterman @EricMeyerowitz @mikejohansenmd @FranciscoMarty_  :: blogs.jwatch.org/hiv-id-observa… [RT]
* Well done, Nick!
* A few reasons - modelling less useful when we get to these numbers; models haven't looked at unknown source cases which are the current concern; qualitative assessment now most important.
* I use them all! twitter.com/simongerman600…
* A special shout-out to our colleagues in occupational medicine, epidemiology, primary care and the army of nurses also contributing. And of course to our public health colleagues for patiently giving us a crash course in government.
* Thanks Greg - there are many of us standing shoulder-to-shoulder (metaphorically) with our public health colleagues, all trying our best to get us all through this difficult time.

=> /tweeters/tweets_kenshirriff.gmi Ken Shirriff (@kenshirriff): Restored Apollo Guidance Compu...
* For IBM 1401 fans, check out this video of an amazing miniature model of a 1401 and peripherals with a vintage narration. My favorite part: tiny cards getting loaded into the card reader with tweezers. twitter.com/vcfederation/s…
* Objectively, how does the 8008 image work as a puzzle? Challenging? Too difficult? Too repetitive? Educational? Do you think a better IC puzzle would scale up, e.g 80x86, or scale down, e.g. 555 timer?
* "antiphlogistic" got my attention too. According to wiktionary, one meaning is "Opposed to the doctrine of phlogiston", so it's definitely a word to work into conversation. (I also just realized I've been mentally scrambling the pronunciation of phlogiston for decades.)
* Looks like an interesting chip. I just ordered one to take a look.

=> /tweeters/tweets_paulg.gmi Paul Graham (@paulg): ...
* Filecoin is already 5.75x cheaper than Amazon S3’s base tier —  :: Filecoin: $0.048/GB/year :: AWS S3: $0.276/GB/year twitter.com/_anishagnihotr… [RT]
* From pre-human times. This is the root of people's problem with economic inequality.
* In places that are strict about corruption by political leaders, it's more common for feckless relatives to conceal it from them, because in that case the relatives are selling out their family member and not just the country.
* No, you can sell them. That's the whole point of a positive-sum game.
* Many of them, though some do start as footnotes.
* Nothing is more likely. The feckless relatives of famous and/or powerful people more often than not try to ride on their coattails in some way. Have you read so little history that this idea seems unfamiliar?
