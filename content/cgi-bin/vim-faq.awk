#!/usr/bin/awk

# Trim functions from https://gist.github.com/andrewrcollins/1592991
function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
function trim(s)  { return rtrim(ltrim(s)); }

# Skip irrelevant lines
/^SECTION/ { next }
/^=======/ { next }

# Identify the main headings
/    faq-[0-9]+$/ { h1=$1; next; print "Heading: " h1}

# Identify the sub-headings
/    faq-[0-9]+\.[0-9]+$/ {

    # If we hit a sub-heading and have a previous answer, then we
    # can see if it matches to print.

    IGNORECASE = 1
    if(question ~ query || (deep && answer ~ query)) {
        
        if(full) {
            print "Q: " question
            print "Answer\n------" answer "\n"
        } else {
            print question
        }
    }
    IGNORECASE = 0

    h2 = $1;
    getline
    question = $0
    while($0 != "") {
        getline
        question = question "\n" $0
    }
    want_answer = 1
    answer = ""

    next;
}

# Identify the questions as n.n. <at least 10 characters> up to
# the next blank line.
/^NEVERNEVER [0-9]+\.[0-9]+\..{10,}/ {
    question=$0;
    next
}

# All other lines are questions or answers (or section markers)
# Answer markers: begin after question-end blank line. End on the next
# sub-heading marker (faq-n.n$), /^[=]+/ or else the end of file.

{
    if(want_answer)
       answer = answer "\n" $0

    #print "What's this line?" 
}

