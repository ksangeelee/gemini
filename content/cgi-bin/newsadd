#!/bin/bash

# Input to this script is taken as a headline to add to the news feed. If it
# begins with the word stored in the file SECRET, then it's added to the live
# news feed, otherwise it's appended to a moderation queue file. Entries in
# here can be moved to the live news file manually. The script stops accepting
# news submissions if the moderation queue grows beyond a given threshold.
#
# File paths are relative to the cgi-bin directory.

NEWS_FILE="../news.gmi"
SECRET_FILE="../../cgi_assets/SECRET"
MODERATION_QUEUE="../../cgi_assets/news_for_moderation.gmi"
MAX_QUEUE_SIZE=250

function urldecode() {
    # Replace-ALL (//) '+' with <space>
    : "${*//+/ }";
    # Replace-ALL (//) '%' with escape-x and evaluate (-e) on echo 
    echo -e "${_//%/\\x}";
}

if [[ "${QUERY_STRING}" == "" ]]; then
    echo -ne "10 Suggest a news headline\r\n"
    exit
fi

echo -ne "20 text/gemini\r\n"

SECRET=$(cat ${SECRET_FILE}|tr -cd '[A-Za-z0-9 _.,\-]')

# Get the url-decoded (and sanitised) version of the query string.
DECODED=$(urldecode "${QUERY_STRING}"|tr -cd '[A-Za-z0-9 _.,]')

# Remove any leading 'secret' moderation code
MODERATED=${DECODED#$SECRET}

QUEUE_LENGTH=$(/usr/bin/wc -l ${MODERATION_QUEUE}|cut -d ' ' -f 1) 

if [[ -f ${MODERATION_QUEUE} && ${QUEUE_LENGTH} -ge ${MAX_QUEUE_SIZE} ]]; then
    echo "I don't think anyone's checking any more, don't waste your time."
    exit
fi

NOW=$(date +'%Y-%m-%d')

# If the line is unchanged, then assume no moderation code
if [[ "${DECODED}" == "${MODERATED}" ]]; then
    echo "## Thanks, I've added it to the moderation queue"
    echo "This means it will be checked and added to the news feed manually."
    echo "=> /news.gmi Back to the news"
    echo "### ${NOW}: ${DECODED}" >> ${MODERATION_QUEUE}
else
    awk "/^###/ && !n {print \"### ${NOW}: ${MODERATED## }\"; n=1} {print}" ../news.gmi >/tmp/news.gmi
    mv -f /tmp/news.gmi ../news.gmi
    # echo "### ${NOW}: ${MODERATED}" >> ${NEWS_FILE}
    echo "Added news to the top of the list"
    cat ${NEWS_FILE}
fi

