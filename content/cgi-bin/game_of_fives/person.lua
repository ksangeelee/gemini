-- Traits define a person type and weights for how they cope with associated
-- tasks. Weight * 10 == percent chance of success at activity
local traits = {
    --             state             e  f  i  s  r
    explorer =  { id = 2, weights = {3, 1, 2, 1, 1}, name = "an explorer" },
    farmer =    { id = 3, weights = {3, 1, 2, 1, 1}, name = "a farmer" },
    inventor =  { id = 4, weights = {3, 1, 2, 1, 1}, name = "an inventor" },
    socialite = { id = 5, weights = {3, 1, 2, 1, 1}, name = "a socialite" },
    apathetic = { id = 6, weights = {0, 1, 1, 1, 3}, name = "a lazy oaf" }
}

states = {
    idle=0, exploring=1, farming=2, inventing=3, socialising=4, resting=5,
    fromId = function(id)
        local names = {
            "Idle", "Exploring", "Farming", "Inventing", "Socialising",
            "Resting"
        }
        return names[id + 1]
    end,
    fromVerb = function(v)
        local verbs = {
            idle=0, explore=1, farm=2, invent=3, socialise=4, rest=5
        }
        return verbs[v]
    end

}

-- Define class Person
Person = {}

function Person:new(name, trait)

    local p = setmetatable({}, { __index = Person })

    p.name = name
    p.trait = traits[trait]

    p.state = states.idle

    p.stats = Stats:new(p)

    return p
end

function Person:applyStats()

    self.stats:applyActivity(self)
end
    
function Person:hasFarmingTool()
    local household = householdOf(self)
    return household:hasFarmingTool()
end

function Person:printSummary()
    print(self.name .. ", residing in " .. householdOf(self).name) 
    print("  Trait:", self.trait.name) 
    print("  Happy:", self.stats.happiness) 
    print("  Tired:", self.stats.tiredness) 
    print("Boredom:", self.stats.boredom) 
    print(" Hunger:", self.stats.hunger) 
    print("Activity", states.fromId(self.state)) 

end

-- End class Person

