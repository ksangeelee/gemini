
-- Return array of strings from 'name' attribute of table elements.
function nameList(t)
    local names = {}
    for i,v in ipairs(t) do
        local name = v["name"]
        if(name ~= nil) then
            table.insert(names, name)
        end
    end

    return names
end

function printHouseholdReport(h)

    for idx, statItem in ipairs({
        { hunger="hungry" },
        { tiredness="tired" },
        { boredom="bored" }
    }) do

        stat, statName = next(statItem)

        local people = nameList(h:memberStat(stat))
        local names = table.concat(people, ", ")

        if(#people > 0) then
            if(#people > 1) then
                print(names .. " are " .. statName)
            else
                print(names .. " is " .. statName)
            end
        else
            print("Nobody is " .. statName)
        end
    end

    print(string.format("\n%20s %5s %5s %5s", "Name", "Hun", "Bor", "Tir"))
    for idx, p in ipairs(h.members) do
        print(string.format("%20s %5d %5d %5d",
            p.name,
            p.stats.hunger, p.stats.boredom, p.stats.tiredness)
        )
    end
end


