-- Define class Household
Household = {}

function Household:new(name, members)

    local h = setmetatable({}, { __index = Household })

    h.name = name
    h.members = members
    h.inventory = {}

    return h
end 

function Household:addInventory(item, qty)

    qty = qty or 1

    if(item ~= nil) then
        local count = self.inventory[item]
        if(count == nil) then
            self.inventory[item] = qty
        else
            self.inventory[item] = count + qty
        end
    end
end

-- Given "hunger", "boredom", "tiredness", return the members
-- where that stat is >= the given threshold (default 1)
function Household:memberStat(statType, threshold)

    if(statType ~= "hunger" and statType ~= "boredom" and 
        statType ~= "tiredness") then
        print("Invalid statType", statType)
        return nil
    end

    threshold = threshold or 1

    local people = {}

    for i, v in ipairs(self.members) do
        if v.stats[statType] >= threshold then
            table.insert(people, v)
        end
    end

    return people
end

function Household:hasFarmingTool()
    local item = self.inventory["hoe"] or 0
    if(item > 0) then
        return true
    end
end

-- End class Household

