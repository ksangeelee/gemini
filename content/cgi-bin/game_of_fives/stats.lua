
local stats = {

    -- Offsets: Alter each turn. Together, these will offset happiness.
    -- The stats should fall in the range of 0 - 100 (e.g. percent)
    --            I     e     f     i     s     l
    tiredness = {-1.0,  1.0,  1.0,  1.0, -1.0, -1.0},
    boredom   = { 1.0, -1.0, -0.5, -1.0, -1.0,  0.5},
    hunger    = { 1.0,  1.0,  1.0,  0.0, -0.5,  0.5}
}

-- Define class Person
Stats = {}

function Stats:new(p)

    local t = setmetatable({}, { __index = Stats })

    t.person = p

    -- Happiness is the overall score
    t.happiness = 0

    -- stats that can affect happiness
    t.tiredness = 0
    t.boredom = 0
    t.hunger = 0

    return t
end

-- This should perhaps be entirely derived from stats...
function Stats:addHappiness(delta)
    self.happiness = self.happiness + delta
end 

-- Modifies stats according to traits and activities (states)
-- If state matches trait, then the stats effect is amplified.
function Stats:applyActivity()

    local trait = self.person.trait 
    local state = self.person.state

    local idx = state + 1

    local multiplier = 1

    if(trait.id == idx) then
        multiplier = 2
    end
    self.tiredness = self.tiredness + stats.tiredness[idx]
    self.boredom = self.boredom + stats.boredom[idx]
    self.hunger = self.hunger + stats.hunger[idx]
end 

