#!/usr/bin/lua5.3

dofile("utils.lua")
dofile("reports.lua")
dofile("person.lua")
dofile("household.lua")
dofile("stats.lua")

print("Game of Fives")

math.randomseed(os.time())

exploreItems = { "wood", "iron", "ruby", "flowers", "coffee" }
farmItems = { "carrots", "potatoes", "onions", "beans", "peppers" }
toolItems = { "hoe" }

findableItems = concatTables(exploreItems, farmItems)
craftableItems = concatTables(toolItems)

allItems = concatTables(exploreItems, farmItems, toolItems)

-- dumpTable(allItems, "allItems")

people = {
    Person:new("Florin the Great", "explorer"),
    Person:new("Blorin the shirker", "apathetic")
}

households = {
    Household:new("The Steading", { people[1], people[2]})
}

-- Find household of person. Perhaps optimise this for lookup.
function householdOf(person)
    for i = 1, #households do
        for k,v in pairs(households[i].members) do
            if(person == v) then
                return households[i]
            end
        end
    end

    return nil
end

-- Perform exploration, possibly in an environment. The chance of finding
-- something useful depends on the environment, and on how keen to explore
-- the person is (their trait).
--
function explore(person, environment)


    local trait = person.trait
    weighting = trait.weights[1] -- 1 is explore weight
    local threshold = weighting * 10

    local percent = math.random(1, 100)

    local item = nil
    if(percent >= threshold) then  -- WIN!
        -- pick a random item
        item = exploreItems[math.random(1, #exploreItems)]
    end

    return item
end

-- Perform farming, possibly in an environment. The chance of producing
-- something useful depends on the environment, and on how keen to farm
-- the person is (their trait).
--
function farm(person, environment)

    local trait = person.trait
    weighting = trait.weights[1] -- 2 is farming weight
    local threshold = weighting * 10

    if(person:hasFarmingTool()) then
        threshold = threshold * 1.5
    end

    local percent = math.random(1, 100)

    local item = nil
    if(percent >= threshold) then  -- WIN!
        -- pick a random item
        item = farmItems[math.random(1, #farmItems)]
    end

    return item
end

-- Perform a turn of the game
function doTurn()

    io.write(".")

    for idx, person in ipairs(people) do

        local household = householdOf(person)
       
        if(person.state == states.idle) then

        elseif(person.state == states.exploring) then

            item = explore(person, nil)

            if(item ~= nil) then
                household:addInventory(item)
            end

        elseif(person.state == states.farming) then

            item = farm(person, nil)

            if(item ~= nil) then
                household:addInventory(item)
            end

        elseif(person.state == states.inventing) then
            -- invent looks for inventory items and may consume
            -- them to make something new.
        end

        -- apply costs of effort
        person:applyStats()
    end
end

local turns = 0

-- Test by reading commands

local f = assert(io.open("test.cmd", "r"))

for line in f:lines() do
    v1,v2 = line:match("^(%d+)%s+(%w+)")
    if(v1 ~= nil and v2 ~= nil) then
        -- set state
        -- print(string.format("'%s' '%s'", v1, v2))
        local state = states.fromVerb(v2) or states.idle
        people[tonumber(v1)].state = state

        --chooseFrom({states.idle,
        --    states.exploring, states.farming, states.inventing,
        --    states.socialising, states.resting
        -- })

    elseif(line:match("^go")) then

        doTurn()
        turns = turns + 1;
    end
end

f:close()

print(" " .. turns .. " Turns complete")

for hIdx,h in ipairs(households) do

    print("Household report for " .. h.name .. "\n")
    printHouseholdReport(h)
    print("\n")

    --[[
    print(h.name .. " hungry members")
    for _, p in pairs(h:memberStat("hungry")) do
        print(p.name)
    end

    print("Inventory of: ", h.name)
    -- Dump inventory
    for i,v in ipairs(allItems) do
        local count = h.inventory[v] or 0
        print(string.format("Item %8s: %d", v, count))
    end
    for i,v in ipairs(h.members) do
        v:printSummary()
    end
    ]]
end

