#!/usr/bin/awk -f

BEGIN {
    block_idx = 0
}

/<.+>/ {

    if(match($0, /\[([0-9]+-[0-9]+-[0-9]+)T([0-9]+:[0-9]+).+] <([^>]+)>(.+)/, arr)) {

        headers = ""

        if(msg_date != arr[1]) {
            msg_date = arr[1]
            headers = "\n## " msg_date "\n"
        }

        if(msg_user != arr[3]) {
            msg_user = arr[3]
            headers = headers "\n### " arr[2] " - " msg_user
        }

        if(headers != "") {
            # store any existing block and start a new one
            if(block != "") {
                blocks[block_idx++] = block
                block = ""
            }
            block = headers "\n"
            # print headers
        }

        block = block "" arr[4] "\n"
        # print "> " arr[4]

        # Print any gemini links
        for (idx=1; idx <= NF; idx++) {
            if($idx ~ /(gemini|http|https):\/\/.+/)
                block = block "=> " $idx " " msg_user "'s link to '" $idx "'\n"
                # print "=> " $idx " " msg_user "'s link to '" $idx "'"
        }
    }
}

END {
    if(block != "")
        blocks[block_idx] = block;

    max_block = block_idx

    while(block_idx >= 0) {
        idx = ( sort == "desc" ? block_idx : max_block - block_idx )
        printf("%s", blocks[idx])
        block_idx--;
    }
}
