#include <sqlite3.h>
#include <stdio.h>

/*
 * Adapted from an example from SQLite C tutorial (C) Jan Bodnar See
 * http://zetcode.com/db/sqlitec/ for the tutorial.
 *
 * Modified by Kevin Sangeelee to read a random record from a database of
 * tweets. The count of records is hard-coded in the query for performance.  If
 * a different database is used, be sure to replace this value, or use a
 * select(count) sub-query instead.
 *
 * Compile:  gcc -o twitter twitter.c -lsqlite3
 *
 * The schama: CREATE TABLE IF NOT EXISTS "tweets"(tweet text not null);
 *
 * That's all there is to tell!
 */

int main(void) {
    
    sqlite3 *db;
    sqlite3_stmt *res;
    
    int rc = sqlite3_open("../../cgi_assets/twitter.db", &db);
    
    if (rc != SQLITE_OK) {
        
        fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        
        return 1;
    }
    
    rc = sqlite3_prepare_v2(db, "select tweet from tweets limit 1 offset abs(random() % 1219978)", -1, &res, 0);    
    
    if (rc != SQLITE_OK) {
        
        fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        
        return 1;
    }    
    
    rc = sqlite3_step(res);
    
    if (rc == SQLITE_ROW) {
        printf("20 text/gemini\r\n");
        printf("#The Twitter Oracle spake thus, the words spoken were:\n\n\n");
        printf("```\n%s\n```\n\n\nJust a tiny sample of what you're missing out on - refresh for more!", sqlite3_column_text(res, 0));
    }
    
    sqlite3_finalize(res);
    sqlite3_close(db);
    
    return 0;
}


