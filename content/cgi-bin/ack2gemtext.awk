#!/usr/bin/awk -f

BEGIN {
    FS = ":";
    filename=""
}

{
    if($1 != filename) {
        print "=> /" $1 " " $1 "\n";
        filename = $1
    }
    print "(Line " $2 "): " $3 "";
}

END {
}
