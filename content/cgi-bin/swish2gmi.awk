#!/usr/bin/awk -f

BEGIN {
    IGNORECASE = 1
#    print "VV " query

    # Remove and, or, near operators
    gsub(/ (or|and|near) |[*]/, " ", query)
    # Remove not clauses and their operand
    gsub(/ not +[^ ]+/, " ", query)
    # Collapse multiple spaces
    gsub(/[ ]{2,}/, " ", query)
    # Remove leading and traling spaces
    gsub(/(^[ ]{1,}|[ ]{1,}$)/, "", query)
    # Replace spaces with '|'
    gsub(/ /, "|",query)

#    print "VV " query

}

# Store the sitemap for lookup
/^=> / {
    sitemap[$2] = $0
    next
}

# Swish++ output lines, identified by their integer rank
/^[0-9]+ / {

    key=substr($2, 2)
    if (sitemap[key]) {
        print sitemap[key] "\n"
    } else {
        print "=> " key "\n"
    }

    count = 0

    while((getline newline < $2) > 0) {
        if( newline ~ query) {
            print "* " newline
            if(++count == 5) {
                print "More than 5 lines match"
                break
            }
        }
    }
    close($2)

    print ""

    next
}

{
    # Default handling, only print lines from stdin
    #  e.g. not from sitemap.gmi which is used to build titles
    if(FILENAME == "-")
        print
}

END {
}

