#!/bin/bash

# This script pulls the gemini.susa.net issued certs from susanet-wp.

cd ~/gemini

rm -Rf ./acmefiles/gemini.susa.net_old >/dev/null 2>&1
mv ./acmefiles/gemini.susa.net ./acmefiles/gemini.susa.net_old
/snap/bin/lxc file pull susanet-wp//root/.acme.sh/gemini.susa.net \
    ./acmefiles \
    --recursive --create-dirs

# Kill the servers using the certificates
pkill gemserv
pkill lighttpd

sleep 1

for p in gemserv lighttpd; do
    if pgrep "${p}"; then
        echo "${p} is still running"
        pkill -9 "${p}"
    fi
done

# Launch gemserv (needs run in background)
./gemserv ./config.toml >gemserv.log &

# Launch lighttpd (daemons itself)
cd ./http
./lighttpd -f lighttpd.conf -m ./lighttpd_libs/

