#!/bin/bash

function urldecode() {
    # Replace-ALL (//) '+' with <space>
    : "${*//+/ }";
    # Replace-ALL (//) '%' with escape-x and evaluate (-e) on echo 
    echo -e "${_//%/\\x}";
}

function urlencode() {
  local string="${1}"
  local strlen=${#string}
  local encoded=""
  local pos c o

  for (( pos=0 ; pos<strlen ; pos++ )); do
     c=${string:$pos:1}
     case "$c" in
        [-_.~a-zA-Z0-9] ) o="${c}" ;;
        * )               printf -v o '%%%02x' "'$c"
     esac
     encoded+="${o}"
  done
  echo "${encoded}"    # You can either set a return variable (FASTER) 
}

IFS='&' read -ra QUERY_PARTS <<< "${QUERY_STRING}"
declare -A QUERY_PARAMS
for p in ${QUERY_PARTS[@]}; do
    IFS='=' read -ra PARAM_PARTS <<< "${p}"
    QUERY_PARAMS[${PARAM_PARTS[0]}]=$(urldecode "${PARAM_PARTS[1]}")
done

URL=${QUERY_PARAMS['url']:-"gemini://gemini.susa.net/"}

ATTEMPTS=0

while [[ $(( ATTEMPTS++ )) < 4 ]]; do



#echo "Attempts = ${ATTEMPTS}"
if [[ ! ${URL} =~ gemini://.+ ]]; then
#    echo "Prepending scheme"
    URL="gemini://${URL}"
fi

if [[ "${URL}" =~ (gemini://)([^:/]+):?([0-9]+)?(/.*)? ]]; then

    SCHEME=${BASH_REMATCH[1]}
    HOST=${BASH_REMATCH[2]}
    PORT=${BASH_REMATCH[3]}
    URLPATH=${BASH_REMATCH[4]}

#    echo "Getting ${URLPATH} from Host ${HOST}, port ${PORT:-1965}"
fi


FIFO="/var/run/user/${EUID}/raw-${$}.gmi"

#echo "FIFO is ${FIFO}"

if [[ "${QUERY_PARAMS['field']}" != "" ]]; then
    FIELD=$(urlencode "${QUERY_PARAMS['field']}")
    URL="${URL}?${FIELD}"
fi

#echo "Fetching with gem-get '${URL}'"

timeout 5 ~/gemini/cgi_assets/gem-get ${URL} | head -c 2048K >${FIFO}

read -n 1000 RESP_CODE META META_EXTRA <${FIFO}

RESPONSE="${RESP_CODE} ${META}"

#echo "Response is ${RESPONSE}"

case "${RESP_CODE}" in
[2]?)
#	echo "Got an OK ${RESP_CODE} of type ${META}"
    if [[ "${META}" =~ ^text/gemini ]]; then
	    cat ${FIFO} | ~/gemini/cgi_assets/proxy_html.awk -v "base=${SCHEME}${HOST}" -v "urlpath=${URLPATH}"
    elif [[ "${META}" =~ ^text/plain ]]; then
        # Remove the header and dump the output
        cat ${FIFO}|tr '\r' '\n' |tail -n "+2"
    else
        cat ${FIFO}|tail -n "+2"
    fi
	break
	;;

[3]?)
#	echo "Got a redirect ${RESP_CODE} to ${META}"
	URL="${META//[$'\t\r\n ']}"
    if [[ ! "${URL}" =~ ^gemini:\/\/ ]]; then
        if [[ "${URL}" =~ ^/ ]]; then
            URL="${SCHEME}${HOST}:${PORT:-1965}${URL}"
        else
            echo "Unsupported redirect: ${URL}"
            echo "You will need to manually copy this link"
            break
        fi
    fi
	;;

[1]?)
#	echo "Got an input request ${RESP_CODE} text ${META}"
	URL="${META}"
    cat ~/gemini/cgi_assets/proxy_input.html | sed \
        -e "s/\${META}/${META} ${META_EXTRA}/" \
        -e "s@\${ACTION}@${SCHEME}${HOST}:${PORT:-1965}${URLPATH}@"
    break
	;;

[5]?)
    echo "The request failed (response: ${RESP_CODE})"
    if [[ "${RESP_CODE}" == "51" ]]; then echo "NOT FOUND"; fi
    if [[ "${RESP_CODE}" == "59" ]]; then echo "BAD REQUEST"; fi
    break
    ;;
*)
	echo "Unhandled response code (${RESP_CODE})"
	break
	;;
esac


done # end of while

if [[ ${ATTEMPTS} -ge 5 ]]; then
    echo "That link was sending me round in circles!"
fi


#echo "Finished with response code ${RESP_CODE}"

rm ${FIFO}

#set

