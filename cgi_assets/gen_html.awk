#!/usr/bin/awk -f

BEGIN {
    IGNORECASE=1
    print("<!doctype html>\n<html lang=en>\n<head>\n<meta charset=utf-8>")
    print("<title>SusaNET Gemini</title>\n")
    print(\
        "<style>\n"\
        "body {\n"\
        "    background-color: #000033;\n"\
        "    color: white;\n"\
        "    font-family: Verdana, Geneva, sans-serif;\n"\
        "}\n"\
        "h1 {\n"\
        "    font-size: 18pt;\n"\
        "    color: #FF9933;\n"\
        "}\n"\
        "h2 {\n"\
        "    font-size: 15pt;\n"\
        "    color: #CC6600;\n"\
        "}\n"\
        "h3 {\n"\
        "    font-size: 12pt;\n"\
        "}\n"\
        "pre {\n"\
        "    padding: 1em;\n"\
        "    background-color: #000010;\n"\
        "}\n"\
        "a:link {color: #EFEFBC}\n" \
        "a:visited {color: #EFEFBC}\n" \
        "a:hover {color: yellow}\n" \
        "</style>\n"\
    )
    print("</head>\n<body>")
    print("<p><a href='https://gemini.circumlunar.space/clients.html'>Get a Gemini reader to view properly.</a> If you're using Windows, then the <a href='https://www.marmaladefoo.com/pages/geminaut'>GemiNaut client, available here</a> is easy to install. If all you have is a shell, then see <a href='https://gemini.susa.net:1993/gemini_prep_kit.html'>The Minimum Gemini Survival Kit</a></p>")
}

/^#[^#]/ {
    print "<h1>" substr($0,3) "</h1>"
    next
}

/^##[^#]/ {
    print "<h2>" substr($0,4) "</h2>"
    next
}

/^###[^#]/ {
    print "<h3>" substr($0,4) "</h3>"
    next
}


/^```/ {
    print "<pre>"

    while(getline line) {

        if(line == "```") {
            print "</pre>"
            next
        } else
            print line
    }
}

/^=>/ {
    if (match($0, /^=>\s+([^ \t]+)\s+(.+)/, a)) {

        link = a[1]
        link_text = a[2]
        gsub(/\.gmi/, ".html", link)
        print "<li><a href='" link "'>" link_text "</a></li>"

    } else if (match($0, /^=>\s+(.+)/, a)) {

        link = a[1]
        gsub(/\.gmi/, ".html", link)
        print "<li><a href='" link "'>" link "</a></li>"
    }
    next
}

/^>.+/ {
    print "<blockquote>" substr($0,2) "</blockquote>"
    next
}

/.+/ {
    print "<p>" $0 "</p>"
}

END {
    print("</body>\n</html>")
}

