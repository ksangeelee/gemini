#!/bin/bash

# This script tries to generate non-common words from the pages in the site.
# It does this by passing words through a spell checker and keeping those
# words that the spell checker rejects. It would be improved with a dictionary
# containing only the most common words. It currently retains only esoteric
# words.

cd /home/kevin/gemini/content

# In case we run in a CGI environment without LC_ALL
#if [[ "$LC_ALL" == "" ]]; then
#    LC_ALL=C
#fi

#echo "LC_ALL is ${LC_ALL}"

#echo "Regenerate the sitemap and Swish++ index..."
#./cgi-bin/sitemap

echo "Generate the site header words"

find . -name '*.gmi' | xargs grep -P '^#{1,2} ' | \
            cut -d':' -f 2- | tr '_' ' '|grep -o -E '\w{3,}' | \
            aspell list | sort | uniq >../cgi_assets/site_hdr_words.txt

echo "Generate the site content words"

#../cgi_assets/search -D >/tmp/t1.txt
#grep -Pi '^[a-z][a-z0-9]{2,}' </tmp/t1.txt >/tmp/t2.txt
#sed -e "s/[-_']/\n/g" </tmp/t2.txt | sort | uniq >/tmp/t3.txt
#aspell list </tmp/t3.txt >/tmp/t4.txt
#sort </tmp/t4.txt |uniq >/tmp/t5.txt
#exit
../cgi_assets/search -D | grep -Pi '^[a-z][a-z0-9]{2,}' | sed -e "s/[-_']/\n/g" | \
    sort | uniq | aspell list | sort | uniq >../cgi_assets/site_words.txt

exit;

# THIS IS THE END OF THE SCRIPT, BELOW REMAINS FOR REFERENCE ONLY

for w in $(find . -name '*.gmi' | xargs grep -P '^#{1,2} ' | \
            cut -d':' -f 2- | tr '_' ' '|grep -o -E '\w{3,}' | \
            sort|uniq)
do
    
    dict -Ca $w >/dev/null 2>&1 || dict -Ca -s lev $w >/dev/null 2>&1
    if [[ "$?" != "0" ]]; then
        echo $w
    else
        echo "Dropped $w" >&2
    fi
done >../cgi_assets/site_hdr_words.txt

exit

for w in $(echo $(../cgi_assets/search -D|grep -P '^[a-z]{3,}'))
do
    dict -Ca -s exact $w >/dev/null 2>&1 || dict -Ca -s lev $w >/dev/null 2>&1
    if [[ "$?" != "0" ]]; then
        echo $w
    fi
done >../cgi_assets/site_words.txt

