#!/bin/bash

cd ~/gemini/content/

for f in *.gmi; do
    HTML_OUT="../http/htdocs/$(basename $f .gmi).html"
    cat $f | ~/gemini/cgi_assets/gen_html.awk >${HTML_OUT}
done

