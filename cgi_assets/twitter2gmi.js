#!/usr/bin/nodejs

/*
 * twitter2gmi.js (C) 2020 Kevin Sangeelee, released under the GPLv2 or later
 * at your option. Please use under the terms of this license or not at all.
 */

const os = require('os')
const fs = require('fs');
const fsp = fs.promises
const path = require('path')
const cheerio = require('cheerio')
const bent = require('bent')
const mri = require('mri')

/*
 * Check for command line parameters
 */
const params = mri(process.argv.slice(2), {
    default: { 'index-page': 'index.gmi' }
})

if(!params['gmi-dir']) {
    console.log("No gemtext output directory specified. Use --gmi-dir=/path/to/dir")
    process.exit()
}

if(params['_'].length == 0) {
    console.log("No twitter usernames given, nothing to do!")
    process.exit()
}

/*
 * Set up some global variables
 */
const tweeters = params['_'];

const homedir = os.homedir()
const gmi_base = params['gmi-dir']
    .replace(/\/+$/, '').replace(/[~]/, homedir)
const index_page = params['index-page']

const twitter_base = 'https://mobile.twitter.com'

/**
 * Fetch a given twitter URL and extract data to the given write-steam.  URL to
 * fetch, write-stream for page, write-stream for index page, header flag (i.e.
 * for page 1, write the user's name and bio, etc.)
 *
 * If this is called more then once, the wantHeader flag can be used to
 * suppress the user's profile header so that only the subsequent tweets are
 * written.
 */
async function urlToGemtext(url, wsp, wsi, wantHeader = true) {

    const getBuffer = bent('buffer')
    let buffer

    try {
        buffer = await getBuffer(url)
    } catch(e) {
        console.error('Failed to fetch URL: ' + e.message)
        return;
    }

    const $ = cheerio.load(buffer.toString('utf8'), {
        normalizeWhitespace: true
    })

    const profile = $('table.profile-details')
    const fullname = $('div.fullname', profile).text().trim()

    if(wantHeader === true) {

        const username = $('div.username', profile).text().replace(/[ \t\n\r]/g, '')
        const profilePic = twitter_base + '/' + path.basename(url) + '/photo'
        const badge = $('a.badge img', profile).attr('alt')
        const location = $('div.location', profile).text().trim()
        const bio = $('div.bio', profile).text().trim()


        wsp.write('# ' + fullname + " (" + username + ") " + (badge ? badge.trim() : '') + '\n')
        wsp.write('Bio: ' +  bio + '\n')
        wsp.write('Location: ' + location + '\n')
        wsp.write('=> ' + profilePic + ' ' + fullname + ' profile pic\n')
        wsp.write('\n')

        // Write the index page entry
        wsi.write('=> /tweeters/tweets_' + path.basename(url) + '.gmi ' 
            + fullname + ' (' + username + '): '
            + bio.substring(0, 30) + '...\n'
        )
    }

    let totalLength = 0

    $('table.tweet').each( (idx, e) => {
        
        let tweetContext = $('div.tweet-reply-context', e).text().replace(/[\n\r\t ]{2,}/g,' ').trim()
        const tweetSocial = $('td.tweet-social-context span.context', e).text().trim()
        const tweetText = $('div.tweet-text', e).text().trim().replace(/[\n\r]{1,}/g,'\n> ')
        const tweetSummary = $('div.tweet-text', e).text().trim().replace(/[\n\r]{1,}/g,' :: ')
        const age = $('td.timestamp a', e).text().trim()
        const threadLink = $('td.meta-and-actions span.metadata a', e).attr('href')

        if(tweetContext == "")
            if(tweetSocial == "")
                tweetContext = fullname + " "
            else
                tweetContext = tweetSocial;

        wsp.write('### ' + (idx + 1) + ": " + tweetContext + ' (' + age + ')\n')
        wsp.write('> ' + tweetText + '\n')

        $('div.tweet-text a.twitter_external_link', e).each((idx,link) => {
            const href = $(link).attr('href')
            const text = $(link).text().trim()
            const url = $(link).data('expanded-url')
            wsp.write('=> ' + href + ' ' + text + '\n')
        })

        if(threadLink != "")
            wsp.write('=> ' + twitter_base + threadLink + ' View conversation\n')

        wsp.write("\n")

        if(totalLength < 750 && wantHeader === true) {
            if(tweetSocial.indexOf('retweeted') >= 0)
                wsi.write('* ' + tweetSummary + ' [RT]\n')
            else
                wsi.write('* ' + tweetSummary + '\n')
            totalLength += tweetSummary.length
        }
    })

    const olderTweetsUrl = $('div.w-button-more a').attr('href')

    return olderTweetsUrl
}

async function processTwitterUser(username, wsIndex) {


    const url = twitter_base + '/' + username
    const gmi_file = gmi_base + '/tweets_' + username + '.gmi'

    let wsPage = fs.createWriteStream(gmi_file, {mode: 0o664})

    console.log('Fetching for', username, 'url', url, 'to file', gmi_file)

    urlToGemtext(url, wsPage, wsIndex)
        .then( async (moreTweets) => {
            wsPage.write('## More tweets: URL ' + moreTweets + "\n\n")
            await urlToGemtext(twitter_base + moreTweets, wsPage, wsIndex, false)
            wsPage.close()
        })
}

/**
 * Iterate over the command-line list of users, and fetch each one.
 */

(async () => {

    const indexFile = gmi_base + '/' + index_page
    let wsIndex = fs.createWriteStream(indexFile + '.tmp', {mode: 0o664})

    const now = new Date();
    await wsIndex.write('# Latest few tweets from your tweeters, fetched ' + 
        now.toDateString() + ' ' +
        now.toLocaleTimeString() +
        '\n')
    
    for(let user of tweeters) {
        await processTwitterUser(user, wsIndex)
        wsIndex.write('\n')
        await new Promise((r) => setTimeout(r, 900));
    }
    wsIndex.close()

    await fsp.rename(indexFile + '.tmp', indexFile)
})()

