#!/bin/bash

####
#
# Run in your content directory, with a list of the files you want to include
# in the file '.atom_include'
#
# e.g.
#   ./telnet_znc_to_irc.gmi
#   ./shell_stuff/bash_notes.gmi
#   ./notes_on_gemserv_cgi.gmi
#
# You could ignore this file and just use *.gmi in the code below to include
# everything in the directory.
#
# Note that the UUID generated isn't really a UUID, just a big random hex
# string, but really the chance of this mattering is vanishingly small.

FEED_TITLE="Circumlunatic Ramblings"
BASE_URL="gemini://gemini.susa.net/"
AUTHOR="kevnsan"

shopt -s extglob

function idFromFile {
    H=$(md5sum "$1")
    echo "${H:0:8}-${H:8:4}-${H:12:4}-${H:16:4}-${H:20:12}"
}

function existingEntry {

    cat atom.xml | awk -v "search=$1" ' \
        /^<entry/ {lines="";want=0} \
        /^<\/entry/ { if(want == 1) { print lines $0 "\n"} } \
        { lines = lines $0 "\n"; if($0 ~ search) want = 1 }'
}

#existingEntry 'xr.ht_patches_notes.gmi'
#exit

UPDATED=$(date --iso-8601='seconds' --utc)

cat <<EOF
<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <title>${FEED_TITLE}</title>
  <link href="${BASE_URL}"/>
  <updated>${UPDATED}</updated>
  <author>
    <name>${AUTHOR}</name>
  </author>
  <id>${BASE_URL}</id>
EOF

for f in $(cat .atom_include|cut -c3-); do

    EXISTING_ENTRY=$(existingEntry ${f})

    if [[ "${EXISTING_ENTRY}" != "" ]]; then
        echo "${EXISTING_ENTRY}"
        continue
    fi

    URL_PATH="${f}"
    UPDATED=$(date --reference="${f}" --iso-8601='seconds' --utc)
    UUID="$(idFromFile "$f")"
    TITLE=$(head -50 "${f}"|grep '^#'|head -1)

    SUMMARY=$(egrep '^[ ]*[a-zA-Z0-9]' ${f}|head -20|tr '\n' ' '|cut -c -500)

    TITLE=${TITLE##*([# ])}

    echo "<entry>"

    echo "    <id>urn:uuid:${UUID}</id>"
    echo "    <title>${TITLE:-$f}</title>"
    echo "    <link href='${BASE_URL}${URL_PATH}' rel='alternate'/>"
    echo "    <summary>${SUMMARY}</summary>"
    echo "    <updated>${UPDATED}</updated>"

    echo "</entry>"
done

echo "</feed>"

