#!/usr/bin/awk -f

/tweet-container/ {
    in_tweet = 1
    print "<br>@" user " tweets (" tweet_time "):<br>\n"
}

/tweet-reply-context/ {
    in_reply = 1
}

/tweet-header/ {
    in_header = 1
}

/"profile-details"/ {
    getline
    profile = 1
}

profile && /<\/table>/ {
    profile = 0
}

in_header && /"timestamp"/ {
    getline
    tweet_time = $0
}

in_header && /<\/tr>/ {
    in_header = 0
}

in_tweet && /<\/tr>/ {
    in_tweet = 0
    in_reply = 0
}

profile {
    print
}

in_tweet {
    print
}
