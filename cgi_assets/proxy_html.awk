#!/usr/bin/awk -f

# URL encoding function from https://gist.github.com/moyashi/4063894
function escape(str, c, len, res) {
    len = length(str)
    res = ""
    for (i = 1; i <= len; i++) {
	c = substr(str, i, 1);
	if (c ~ /[0-9A-Za-z]/)
	    res = res c
	else
	    res = res "%" sprintf("%02X", ord[c])
    }
    return res
}

BEGIN {
    IGNORECASE=1
    RS="\r?\n"

    # Lookup table for url encoding:
    for (i = 0; i <= 255; i++) ord[sprintf("%c", i)] = i

    # Skip the response-code line.
    getline

    proxy="https://gemini.susa.net:1993/cgi-bin/proxy.sh"

    print("\n<!doctype html>\n<html lang=en>\n<head>\n<meta charset=utf-8>\n")
    print("\n<title>",base, urlpath,"</title>\n")
    print("<style type='text/css'>")
    while (getline < "../proxy.css")
        print
    print("</style>")
    #print( "<link rel='stylesheet' href='../proxy.css'>")
    print("</head>\n<body>")
    print(" \
          <div class='nav'> \
            <a href='" proxy "'><div>Home</div></a> \
            <a href='" proxy "?url=gemini://gus.guru/'><div>GUS</div></a> \
            <a href='" proxy "?url=gemini://gemini.circumlunar.space/'><div>Project Gemini</div></a> \
            <a href='/proxy.html'><div>URL</div></a> \
          </div> \
          ")
    sub(/\/[^/]*$/, "", urlpath)
}

# Filter HTML tag openings within the Gemtext input
{
    gsub(/[<]/,"\\&lt;", $0)
}

/^#[^#]/ {
    gsub(/^#\s*/, "", $0)
    print "<h1>" $0 "</h1>"
    next
}

/^##[^#]/ {
    gsub(/^##\s*/, "", $0)
    print "<h2>" $0 "</h2>"
    next
}

/^###[^#]/ {
    gsub(/^###\s*/, "", $0)
    print "<h3>" $0 "</h3>"
    next
}


/^```/ {
    print "<pre>"

    while(getline) {

        gsub(/[<]/,"\\&lt;", $0)

        #print "STRING0 is '" $0 "'"
        if($0 ~ /^```/) {
            print "</pre>"
            next
        } else {
            print
        }
    }
}

# Do we have a link relative to the URL path.
/^=>\s*/ {

    if (match($0, /^=>\s*([^ \t]+)(\s+(.+))?/, a)) {
        link = a[1]
        link_text = a[3]

        while(sub(/\/\.\//, "/", link));
        while(sub(/^\.\.\//, "/", link));
        while(sub(/^\/\.\./, "", link));
        while(sub(/\/[^/]+\/\.\./, "", link));
        gsub(/\.\//, "", link)

        if(link ~ /^\/\//) {
            link = "gemini:" link
        } else if(link ~ /^\//) {
            link = base link
        } else if(link !~ /^\w+:/) {
            link = base urlpath "/" link
        }

    }

    if(link_text == "")
        link_text = link

    if(link ~ /^gemini:\/\//) {
       link = proxy "?url=" escape(link)
    }

    print "<div class='link'><a href='" link "'>" link_text "</a></div>"

    link = ""
    link_text = ""

    next
}

/^>.+/ {
    print "<blockquote>" substr($0,2) "</blockquote>"
    next
}

/.+/ {
    print "\n<p>" $0 "</p>\n"
}

END {
    print("</body>\n</html>")
}

